export class Api{
    constructor(){

    }  

    async _age(message){   
        let name = 'sylvain';

        if(message==undefined){
            message = '--age-sylvain'
        }
    
        message.split(' ').forEach((w)=>{
            if(w.includes('--age-')){
                let n = w.split('--age-')[1];
                if(n!=''){
                    name = n.toLowerCase();
                }
            }
        });
    
        let get_age = `https://api.agify.io/?name=${name}`; 
           
        let r = await htr(get_age);
    
        name = name.charAt(0).toUpperCase() + name.slice(1);
    
        return `The average age for the name <b>${name}</b> is <b>${r.age}</b> years old !`;    
    }
    
    async _joke(message){
        let get_joke = `https://v2.jokeapi.dev/joke/Any?type=single`;
    
        let r = await htr(get_joke);
    
        return r.joke;
    }
    
    async _dog(message){
        let get_dog = `https://dog.ceo/api/breeds/image/random`;
    
        let r = await htr(get_dog);
    
        return `<img class="message__image" src="${r.message}">`;
    }
}

function htr(url){
    return new Promise((resolve,reject)=>{
        fetch(url).then((res) => res.json())
        .then((json) => {
            resolve(json);
            return json;
        })
        .catch(err => console.log(err));
    });
}