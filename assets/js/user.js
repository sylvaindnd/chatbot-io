export let me = undefined; // self user
export function setMe(x){ 
    me = x;
}

export let bots = []; // list bots
export function setBots(x){
    bots = x;
}

export class User{
    constructor(id, name, avatar, type){        
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.type = type;
    }

    updateUser(id, name, avatar, type){
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.type = type;
    }

    renderUser(type=this.type){
        let label = '<span class="contact_active rounded py-1 px-1 fst-italic text-light"></span>';
        if(type=='bot'){
            label = '<span class="contact_bot rounded py-1 px-2 fst-italic text-light">bot</span>';
        }else if(type=="offline"){
            label = '<span class="contact_absent rounded py-1 px-1 fst-italic text-light"></span>';   
        }
        let content = 
        `<div id="contact-${this.id}" class="contact d-flex align-items-center rounded text-light p-2 mb-2 position-relative">
            <img src="${this.avatar}" class="rounded-circle" alt="avatar">
            <span class="contact_pseudo">${this.name}</span>
            ${label}
        </div>`;
        return content;
    }

    getInfo(){
        return {
            id:this.id,
            user:this.name,
            profil:this.avatar
        }
    }
    getId(){
        return this.id;
    }
    getUser(){
        return this.name;
    }
    getProfil(){
        return this.avatar;
    }

}