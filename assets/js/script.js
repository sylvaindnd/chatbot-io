import { Interface } from './interface.js';
import { User, setMe, me, setBots, bots } from './user.js';
import { Message } from './message.js';
import { Api } from './api.js';
import { Command } from './command.js';

// Render Interface
let i = new Interface('main','Chatbot.io');
i.render();

// Create Bots 
let b0 = new User(randomString(16), 'YourAge', 'https://cdn.pixabay.com/photo/2017/12/31/15/56/portrait-3052641_960_720.jpg', 'bot');
let b1 = new User(randomString(16), 'Joker', 'https://cdn.pixabay.com/photo/2020/08/13/16/25/joker-5485787_960_720.jpg', 'bot');
let b2 = new User(randomString(16), 'Doggo', 'https://cdn.pixabay.com/photo/2016/11/19/15/20/dog-1839808_960_720.jpg', 'bot');
let b3 = new User(randomString(16), 'Helper', 'https://cdn.pixabay.com/photo/2016/01/19/07/35/strength-1148029_960_720.jpg', 'bot');
i.displayContact(b0.id, b0.renderUser());
i.displayContact(b1.id, b1.renderUser());
i.displayContact(b2.id, b2.renderUser());
i.displayContact(b3.id, b3.renderUser());
setBots([b0,b1,b2,b3]);

// Create Self
let self = new User(randomString(16), 'Sylvain', 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png', 'online');
setMe(self);
i.displayContact(self.id, self.renderUser());

/*
let message = new Message(0, self.id, self.name, self.avatar, '11:11 12/05/2022', 'coucou');
i.displayMessage(message.id, message.renderMessage());
*/

/*
let a = new Api();
let j = await a._age();
console.log(j)
*/

/*
let c = new Command();
let m = await c.commands_request('--hello @all');
console.log(m)
*/

// functions
export function randomString(length=16) {
    let result = '';
    let c = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for (var i = length; i > 0; --i) result += c[Math.floor(Math.random() * c.length)];
    return result;
}

export function formatDateMessage(d){
    return addZero(d.getHours())+':'+addZero(d.getMinutes())+', '+addZero(d.getDate())+'/'+addZero((d.getMonth()+1))+'/'+d.getFullYear();
}

export function addZero(i){
    if(i<10){
        return '0'+i;
    }
    return ''+i;
}