const express =  require('express')
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

app.use(express.static('public'));
app.use('/css',express.static(`${process.cwd()}/public/css`));
app.use('/js',express.static(`${process.cwd()}/public/js`));

console.log(`${process.cwd()}/public/js`)

app.get('/', (req, res) => {
    res.sendFile(`${process.cwd()}/views/index.html`);    
});


io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('disconnect', () => {
        console.log('user disconnected');
    });

    socket.on('chat',async (m) => {

        io.emit('chat', m);

        console.log(m)
        
    });
});
  
server.listen(5600, () => {
    console.log('listening on port : 5600');
});