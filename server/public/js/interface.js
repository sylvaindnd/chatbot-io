import { me } from './user.js';
import { Message } from './message.js';
import { formatDateMessage, randomString } from './script.js';
import { Command } from './command.js';
const socket = io();

export class Interface {
    constructor(main='main', title='Chatbot.io'){
        this.title = title;
        this.main = document.getElementById(main);
    }

    render(){        
        this.main.innerHTML = this.appContainer();    
        this.event();   
        this.unstoreMessage();        
    }

    appContainer(){
        let container =  
        `<div id="app" class="d-flex">
            ${this.navbarContainer()}
            ${this.contactContainer()}
            ${this.messengerContainer()}
        </div>`;
        return container;
    }

    navbarContainer(){
        let container =
        `<nav id="navbar" class="navbar navbar-dark bg-dark fixed-top">
            <div class="container-fluid">
                <a class="navbar-brand">${this.title}</a>
            </div>
        </nav>`;
        return container;
    }

    contactContainer(){
        let container = 
        `<div id="contact_close" class="active">
            <button class="btn btn-outline-secondary" type="button"> <i class="bi bi-arrow-bar-right"></i></button>               
        </div>
        <div id="contacts" class="active col-12 col-sm-4 d-flex flex-column p-2"></div>`;
        return container;
    }

    messengerContainer(){
        let container = 
        `<div id="messenger">
            <div class="messages d-flex col-12 flex-column align-items-center"></div>
            ${this.messangerForm('Enter new message...')}
        </div>`;
        return container;
    }

    messangerForm(placeholder){
        let form = 
        `<form class="new_message d-flex col-12 justify-content-center">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="${placeholder}" aria-label="Enter new message" aria-describedby="button_new_message">
                <button class="btn btn-outline-secondary" type="button" id="button_new_message"><i class="bi bi-send"></i></button>
            </div>
        </form>`;
        return form;
    }   

    displayContact(id, content){
        let contactsContainer = document.getElementById('contacts');
        let c = contactsContainer.querySelector(`#contact-${id}`);
        if(c==undefined||c==null){
            contactsContainer.innerHTML += content;
        }else{
            c.outerHTML = content;
        }       
    }

    displayMessage(id, content){
        let messageContainer = document.querySelector('#messenger > .messages');
        let m = messageContainer.querySelector(`#message-${id}`);
        if(m==undefined||m==null){
            messageContainer.innerHTML += content;
        }
        messageContainer.scrollTop = messageContainer.scrollHeight;
    }

    async newMessage(m='',issocket=false){
        let messageDate = new Date();    

        if(m==''){
            return;
        }
       
        if(issocket){
            let message = new Message(-1, m.userid, m.user, m.avatar, formatDateMessage(messageDate), m.message);  
            this.displayMessage(message.id, message.renderMessage());
            return;
        }       

        socket.emit('chat', {            
            'message':m,
            'userid':me.id,
            'user': me.name,
            'avatar': me.avatar,
        });

        let message = new Message(-1, me.id, me.name, me.avatar, formatDateMessage(messageDate), m);  
        this.displayMessage(message.id, message.renderMessage());

        if(m.includes('--')){
            let c = new Command();
            let rm = await c.commands_request(m);
            rm.forEach((b)=>{
                let bm = b.message.replace(/\n/g,'<br>');
                let message = new Message(-1, b.id, b.user, b.profil, formatDateMessage(messageDate), bm);  
                this.displayMessage(message.id, message.renderMessage());
            });          

            return;
        }            
        
    }    

    unstoreMessage(){
        let messages = localStorage.getItem('message');

        if(messages==null){
            messages = [];
        }else{
            messages = JSON.parse(messages);
        }

        messages.forEach((m)=>{
            this.displayMessage(randomString(16), m);
        });
    }


    event(){
        let messageForm = document.querySelector('form.new_message');  
        let contacts = document.querySelector('#contacts .contact');
        let self = this;

        messageForm.addEventListener('submit',(e)=>{          
            e.preventDefault();  
            let input = messageForm.querySelector('input.form-control');
            let messageText = input.value;     
            input.value = '';       
            this.newMessage(messageText);            
        });

        messageForm.querySelector('#button_new_message').addEventListener('click',(e)=>{          
            e.preventDefault();  
            let input = messageForm.querySelector('input.form-control');
            let messageText = messageForm.querySelector('input.form-control').value;          
            input.value = '';    
            this.newMessage(messageText);            
        });    

        socket.on('chat', function(m) {      
            if(me.id!=m.userid){
                self.newMessage(m,true);
            }             
        });
    }
}