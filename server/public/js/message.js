import { me } from './user.js';
import { randomString } from './script.js';

export class Message{
    constructor(id, userId, userName, userAvatar, messageDate, messageText){

        if(id==-1){
            this.id = randomString(16);
        }else{
            this.id = id;
        }
        
        this.userId = userId;
        this.userName = userName;
        this.userAvatar = userAvatar;
        this.messageDate = messageDate;
        this.messageText = messageText;
    }

    renderMessage(){
        let direction = 'message_other';
        if(me.id==this.userId){
            direction = 'message_me';
        }
        let content = 
        `<div id="message-${this.id}" class="message ${direction} d-flex mt-3">
            <div class="d-flex py-3">                    
                <div class="message_image mx-3">
                    <img src="${this.userAvatar}" class="rounded-circle" alt="avatar">
                </div>
                <div class="message_container">
                    <div class="message_container__pseudo">
                        <span class="message_pseudo__pseudo text-light fw-bold">${this.userName}</span>
                        <span class="message_pseudo__date">${this.messageDate}</span>
                    </div>
                    <div class="message_container__message">
                        <span>${this.messageText}</span>
                    </div>
                </div>
            </div>                
        </div>`;
        this.storeMessage(content);
        return content;
    }

    storeMessage(m){
        let messages = localStorage.getItem('message');

        if(messages==null){
            messages = [];
        }else{
            messages = JSON.parse(messages);
        }

        messages.push(m);

        let str = JSON.stringify(messages);

        localStorage.setItem('message', str);
    }
}