import { User, setMe } from './user.js';
import { randomString } from './script.js';

export function init(){
    event();
}

function event(){
    document.querySelector('#username .username__input_container button').addEventListener('click', ()=>{
        let u = document.querySelector('#username__input').value;
        if(u.length<3){
            u = 'Username' + randomInt(1000, 9999);
        }
        let self = new User(randomString(16), u, 'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png', 'online');
        setMe(self);
        document.querySelector('#username').outerHTML = '';
    });
}

function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
  
