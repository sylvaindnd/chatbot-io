# chatbot-io

## Basic Overview

chatbot-io is a simple chat application that allows users to send and receive messages. This application also allows users to send commands in order for a bot to respond. It uses APIs.

#

## Simple Usage

In new folder : 

```
https://gitlab.com/sylvaindnd/chatbot-io.git
```

In the **server** folder of the chatbot-io project :


```
npm run start
```

In a web browser go to [http://localhost:5500/](http://localhost:5500/) and enjoy.

#

## Structure

```
chatbot-io/
├─ server/
│  ├─ node_modules/
│  │  ├─ ...
│  ├─ props/
│  │  ├─ app.js
│  ├─ public/
│  │  ├─ css/
│  │  │  ├─ ...
│  │  ├─ js/
│  │  │  ├─ ...
│  ├─ views/
│  │  ├─ index.html
├─ README.md
```

#

## Commands
- **--hello** to say hello (@)
- **--yo** to say yo (@)
- **--welcome** to say hello (@)
- **--hi** to say yo (@)
- **--age** find an age from a name : **_--age-{name}_**
- **--joke** a funny little joke
- **--dog** send a dog picture
- **--ping** pong ! (@)
- **--help** understand the commands
- **--h** understand the commands 

> You must specify the name of a bot or **@all** for commands with **@**. ex: "**@Helper --hello**".

#

## New Commands

- To add new commands, go to the **public/js/command.js** file in node server folder. The commands are controlled by the variable **commands**, it is an object type.
- The commands must be added in this format : 
```javascript
const commands = {
    'new_command':{
        res: 'response of new_command',
        for: false,
        des: 'description of new_command'       
    }
}
```
> "**res**" must return the reponse as a string.

> "**for**" allows you to specify if you want to @ a bot or not. if **for: _false_** = the user will have to @ any bot. if **for: _[id,..]_** = the user doesn't need to @ bots, the bots will respond if their ID is in the **for** list.

> "**des**" must return the description as a string.

- Some commands can be the duplication of other, use this format: 
```javascript
const commands = {
    'new_command2':{
        to: 'new_command'     
    }
}
```
> "**to**" is used to retrieve the parent command.

#

## Bots

The POO management of the bots is managed on the client side in **public/js/user.js**.